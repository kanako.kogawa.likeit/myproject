package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.ItemDataBeans;

public class ItemDAO {

	//文字色変更の全件取得
	public static ArrayList<ItemDataBeans> getColorItem()  {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item WHERE type=1;");
			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static ArrayList<ItemDataBeans> getUsedupItem()  {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item WHERE type=2;");

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setType(rs.getInt("type"));
				itemList.add(item);
			}
			return itemList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//IDからアイテムを取得
		public static ItemDataBeans getItemDataBeansById(int itemID)  {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM item where id=?;");
				st.setInt(1, itemID);

				ResultSet rs = st.executeQuery();
				ItemDataBeans item = new ItemDataBeans();
				while (rs.next()) {
					item.setId(rs.getInt("id"));
					item.setName(rs.getString("name"));
					item.setDetail(rs.getString("detail"));
					item.setPrice(rs.getInt("price"));
					item.setFileName(rs.getString("file_name"));
					item.setType(rs.getInt("type"));
				}
				return item;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}


		public static ArrayList<ItemDataBeans> getColorItembyUserId(int userId)  {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				String sql="SELECT id,name,price,file_name," +
						" (SELECT COUNT(*) FROM item i2" +
						" INNER JOIN exchange_detail exd" +
						" ON i2.id = exd.item_id" +
						" INNER JOIN  exchange ex" +
						" ON ex.id = exd.exchange_id" +
						" WHERE ex.user_id = ?" +
						" AND i.id = i2.id" +
						" ) as count" +
						" FROM item i" +
						" WHERE type=1;";

				st = con.prepareStatement(sql);
				st.setInt(1, userId);

				ResultSet rs = st.executeQuery();

				ArrayList<ItemDataBeans> itemList=new ArrayList<ItemDataBeans>();
				while (rs.next()) {
					ItemDataBeans item = new ItemDataBeans();
					item.setId(rs.getInt("id"));
					item.setName(rs.getString("name"));
					item.setFileName(rs.getString("file_name"));
					item.setPrice(rs.getInt("price"));
					item.setCount(rs.getInt("count"));
					itemList.add(item);
				}
				return itemList;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}



}
