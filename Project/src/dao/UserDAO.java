package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.ItemDataBeans;
import beans.RankingDataBeans;
import beans.UserDataBeans;
import beans.UserPossessedDataBeans;


public class UserDAO {


	// ログインIDとパスワードに紐づくユーザ情報を返す
	public UserDataBeans getUserId(String inputLoginId, String inputPassword) {
		Connection con = null;
		UserDataBeans udb=new UserDataBeans();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?;";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, inputLoginId);
			st.setString(2, inputPassword);
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				return udb;
			}
			else {
				return null;
			}
		} catch (SQLException e) {
			//エラーメッセージを出すメソッド
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}



	//ユーザIDに基づくユーザ情報を返す
	public static UserDataBeans getUserDataBeansbyUserId(int id)  {
		UserDataBeans udb=new UserDataBeans();
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql1="SELECT * FROM user WHERE id=?;";
			PreparedStatement st1=con.prepareStatement(sql1);
			st1.setInt(1, id);
			ResultSet rs1=st1.executeQuery();
			while(rs1.next()) {
				udb.setLoginId(rs1.getString("login_id"));
				udb.setName(rs1.getString("name"));
				udb.setPossessedScore(rs1.getInt("possessed_score"));
			}

			String sql2="SELECT * FROM ranking_easy WHERE user_id=?;";
			PreparedStatement st2=con.prepareStatement(sql2);
			st2.setInt(1, id);
			ResultSet rs2=st2.executeQuery();
			while(rs2.next()) {
				udb.setEasyHighScore(rs2.getInt("easy_high_score"));
			}

			String sql3="SELECT * FROM ranking_hard WHERE user_id=?;";
			PreparedStatement st3=con.prepareStatement(sql3);
			st3.setInt(1, id);
			ResultSet rs3=st3.executeQuery();
			while(rs3.next()) {
				udb.setHardHighScore(rs3.getInt("hard_high_score"));
			}
			st1.close();
			st2.close();
			st3.close();
			return udb;
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}



	//	//ユーザidからユーザ情報を取得（マイぺージにて利用）
	//	public UserDataBeans getUserDataBeansbyUserId(int id)  {
	//		UserDataBeans udb=new UserDataBeans();
	//		Connection con=null;
	//		try {
	//			con=DBManager.getConnection();
	//			String sql=
	//					"SELECT user.login_id,user.name,user.possessed_score,ranking_easy.easy_high_score,ranking_hard.hard_high_score" +
	//							" FROM (ranking_easy INNER JOIN ranking_hard ON ranking_easy.user_id=ranking_hard.user_id)" +
	//							" INNER JOIN user" +
	//							" ON user.id=ranking_hard.user_id" +
	//							" WHERE user.id=?;";
	//			PreparedStatement st=con.prepareStatement(sql);
	//			st.setInt(1, id);
	//			ResultSet rs=st.executeQuery();
	//			while(rs.next()) {
	//				udb.setLoginId(rs.getString("login_id"));
	//				udb.setName(rs.getString("name"));
	//				udb.setEasyHighScore(rs.getInt("easy_high_score"));
	//				udb.setHardHighScore(rs.getInt("hard_high_score"));
	//				udb.setPossessedScore(rs.getInt("possessed_score"));
	//			}
	//			st.close();
	//			return udb;
	//
	//		}catch (SQLException e) {
	//			e.printStackTrace();
	//			return null;
	//		} finally {
	//			if (con != null) {
	//				try {
	//					con.close();
	//				} catch (SQLException e) {
	//					e.printStackTrace();
	//					return null;
	//				}
	//			}
	//		}
	//
	//	}

	// ログインIDが半角英数字で4文字以上入力されているか検証
	public boolean isLoginIdValidation(String inputLoginId) {
		if (inputLoginId.matches("[0-9a-zA-Z]{4,}")) {
			return true;
		}
		return false;
	}

	//名前が2文字以上入力されているか検証
	public boolean isNameValidation(String inputName) {
		if(inputName.length()>=2) {
			return true;
		}
		return false;
	}

	//パスワードが8文字以上入力されているか検証
	public boolean isPasswordValidation(String inputPassword ,String confirmPassword) {
		if(inputPassword.length()>=8&&confirmPassword.length()>=8) {
			return true;
		}
		return false;
	}

	//重複するログインIDがないかユーザidをもとに検証
	public boolean isOverlapLoginIdById(int getId,String inputLoginId) {
		boolean isOverlap=false;
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String sql="SELECT login_id FROM user WHERE login_id=? AND id!=?;";
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, inputLoginId);
			st.setInt(2, getId);
			ResultSet rs=st.executeQuery();
			if(rs.next()) {
				isOverlap=true;
			}

		}catch (SQLException e) {
			e.printStackTrace();
			return isOverlap;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return isOverlap;
				}
			}
		}
		return isOverlap;

	}

	//パスワード以外を更新
	public void updateUserWithoutPassword(String inputLoginId,String inputUserName,int id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String sql="UPDATE user SET login_id=?, name=? where id=?;";
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, inputLoginId);
			st.setString(2, inputUserName);
			st.setInt(3, id);
			st.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}


	}

	//パスワード含む情報を更新
	public void updateUser(String inputLoginId,String inputPassword,String inputUserName,int id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String sql="UPDATE user SET login_id=?, name=?, password=? where id=?;";
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, inputLoginId);
			st.setString(2, inputUserName);
			st.setString(3, inputPassword);
			st.setInt(4, id);
			st.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	//新規登録の際、同じログインidがないか検証
	public boolean isOverlapLoginId(String inputLoginId) {
		boolean isOverlap=false;
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String sql="SELECT login_id FROM user WHERE login_id=?";
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, inputLoginId);
			ResultSet rs=st.executeQuery();
			if(rs.next()) {
				isOverlap=true;
			}

		}catch (SQLException e) {
			e.printStackTrace();
			return isOverlap;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return isOverlap;
				}
			}
		}
		return isOverlap;
	}
	//
	//	//新規登録
	//	public void signup(String inputLoginId, String inputUserName, String inputPassword) {
	//		Connection con=null;
	//		con=DBManager.getConnection();
	//
	//		try {
	//			con.setAutoCommit(false);
	//			//ECのBuyDAOを参考に書き換える！
	//			String sql="BEGIN;";
	//			sql+=" INSERT INTO user (login_id,name,password) VALUES (?,?,?);";
	//			sql+=" INSERT INTO ranking_easy (user_id) select id from user where login_id=?;";
	//			sql+=" INSERT INTO ranking_hard (user_id) select id from user where login_id=?;";
	//			sql+=" COMMIT;";
	//			PreparedStatement st=con.prepareStatement(sql);
	//			st.setString(1, inputLoginId);
	//			st.setString(2, inputUserName);
	//			st.setString(3, inputPassword);
	//			st.setString(4, inputLoginId);
	//			st.setString(5, inputLoginId);
	//			st.executeUpdate();
	//			con.commit();
	//			st.close();
	//
	//		}catch (SQLException e) {
	//			e.printStackTrace();
	//		} finally {
	//			if (con != null) {
	//				try {
	//					con.close();
	//				} catch (SQLException e) {
	//					e.printStackTrace();
	//				}
	//			}
	//		}
	//
	//	}

	public int insertUser(String inputLoginId, String inputUserName, String inputPassword) {
		Connection con=null;
		PreparedStatement st = null;
		int autoIncKey=-1;
		try {

			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO user (login_id,name,password) VALUES (?,?,?);",
					Statement.RETURN_GENERATED_KEYS);
			st.setString(1,inputLoginId);
			st.setString(2,inputUserName);
			st.setString(3,inputPassword);
			st.executeUpdate();
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			st.close();

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}return autoIncKey;
	}

	public void addData(int userId) {
		Connection con=null;
		con=DBManager.getConnection();

		try {
			String sql1="INSERT INTO ranking_easy (user_id,easy_high_score,date) VALUES (?,0,now());";
			PreparedStatement st1=con.prepareStatement(sql1);
			st1.setInt(1, userId);
			st1.executeUpdate();
			st1.close();

			String sql2="INSERT INTO ranking_hard (user_id,hard_high_score,date) VALUES (?,0,now());";
			PreparedStatement st2=con.prepareStatement(sql2);
			st2.setInt(1, userId);
			st2.executeUpdate();
			st2.close();
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	//現在所持しているカラーアイテムの取得
	public static ArrayList<ItemDataBeans> getPossessedColorItemsByUserId(int userId) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		try {
			String sql="SELECT i.id,i.name,i.file_name,p.amount,i.type FROM item_possessed p "
					+ " INNER JOIN item i ON p.item_id=i.id WHERE p.user_id=? AND i.type=1;";
			st=con.prepareStatement(sql);
			st.setInt(1, userId);
			ResultSet rs=st.executeQuery();

			ArrayList<ItemDataBeans> possessedItemList =new ArrayList<ItemDataBeans>();
			while(rs.next()) {
				ItemDataBeans item=new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setFileName(rs.getString("file_name"));
				item.setAmount(rs.getInt("amount"));
				item.setType(rs.getInt("type"));
				possessedItemList.add(item);
			}
			return possessedItemList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//現在所持している使い切りアイテムの取得
	public static ArrayList<ItemDataBeans> getPossessedUsedupItemsByUserId(int userId) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		try {
			String sql="SELECT i.id,i.name,i.file_name,p.amount,i.type FROM item_possessed p "
					+ " INNER JOIN item i ON p.item_id=i.id WHERE p.user_id=? AND i.type=2;";
			st=con.prepareStatement(sql);
			st.setInt(1, userId);
			ResultSet rs=st.executeQuery();

			ArrayList<ItemDataBeans> possessedItemList =new ArrayList<ItemDataBeans>();
			while(rs.next()) {
				ItemDataBeans item=new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setFileName(rs.getString("file_name"));
				item.setAmount(rs.getInt("amount"));
				item.setType(rs.getInt("type"));
				possessedItemList.add(item);
			}
			return possessedItemList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザIDを基に所持アイテム情報を返す
	public static boolean isExistPossessedItemByUserId(int userId,int itemId) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		boolean result=false;
		try {
			String sql="SELECT * FROM item_possessed WHERE user_id=? And item_id=?;";
			st=con.prepareStatement(sql);
			st.setInt(1,userId);
			st.setInt(2, itemId);
			ResultSet rs=st.executeQuery();
			if(rs.next()) {
				result=true;
			}
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザIDを基に所持アイテム情報を返す
	public static UserPossessedDataBeans getUserPossessedDataBeansByUserIdAndItemId(int userId,int itemId) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		UserPossessedDataBeans updb=new UserPossessedDataBeans();
		try {
			String sql="SELECT * FROM item_possessed WHERE user_id=? AND item_id=?;";
			st=con.prepareStatement(sql);
			st.setInt(1,userId);
			st.setInt(2, itemId);
			ResultSet rs=st.executeQuery();
			if(rs.next()) {
				updb.setId(rs.getInt("id"));
				updb.setItemId(rs.getInt("item_id"));
				updb.setUserId(rs.getInt("user_id"));
				updb.setAmount(rs.getInt("amount"));
			}
			System.out.println("select UserPossessedDataBeans has been completed");
			return updb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ランキング（かんたん）の全件取得
	public static ArrayList<RankingDataBeans> getEasyRanking() {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		ArrayList<RankingDataBeans> rankingList=new ArrayList<RankingDataBeans>();
		try {
			String sql="SELECT ranking_easy.*,user.name FROM ranking_easy "
					+ " INNER JOIN user ON ranking_easy.user_id=user.id ORDER BY easy_high_score DESC;";
			st=con.prepareStatement(sql);
			ResultSet rs=st.executeQuery();
			while(rs.next()) {
				RankingDataBeans redb=new RankingDataBeans();
				redb.setId(rs.getInt("id"));
				redb.setUserId(rs.getInt("user_id"));
				redb.setUserName(rs.getString("name"));
				redb.setScore(rs.getInt("easy_high_score"));
				redb.setDate(rs.getTimestamp("date"));
				rankingList.add(redb);
			}
			return rankingList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ランキング（むずかしい）の全件取得
		public static ArrayList<RankingDataBeans> getHardRanking() {
			Connection con=null;
			con=DBManager.getConnection();
			PreparedStatement st=null;
			ArrayList<RankingDataBeans> rankingList=new ArrayList<RankingDataBeans>();
			try {
				String sql="SELECT ranking_hard.*,user.name FROM ranking_hard "
						+ " INNER JOIN user ON ranking_hard.user_id=user.id ORDER BY hard_high_score DESC;";
				st=con.prepareStatement(sql);
				ResultSet rs=st.executeQuery();
				while(rs.next()) {
					RankingDataBeans redb=new RankingDataBeans();
					redb.setId(rs.getInt("id"));
					redb.setUserId(rs.getInt("user_id"));
					redb.setUserName(rs.getString("name"));
					redb.setScore(rs.getInt("hard_high_score"));
					redb.setDate(rs.getTimestamp("date"));
					rankingList.add(redb);
				}
				return rankingList;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}finally {
				if(con!=null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	//ログインされたユーザIDを基にユーザ削除
	public static void userDelete(int userId){
		Connection con=null;
		con=DBManager.getConnection();
		try {
			PreparedStatement st1=null;
			String sql1="DELETE FROM user WHERE id=?";
			st1=con.prepareStatement(sql1);
			st1.setInt(1, userId);
			st1.executeUpdate();
			
			PreparedStatement st2=null;
			String sql2="DELETE FROM ranking_easy WHERE user_id=?";
			st2=con.prepareStatement(sql2);
			st2.setInt(1, userId);
			st2.executeUpdate();
			
			PreparedStatement st3=null;
			String sql3="DELETE FROM ranking_hard WHERE user_id=?";
			st3=con.prepareStatement(sql3);
			st3.setInt(1, userId);
			st3.executeUpdate();
			
			PreparedStatement st4=null;
			String sql4="DELETE FROM item_possessed WHERE user_id=?";
			st4=con.prepareStatement(sql4);
			st4.setInt(1, userId);
			st4.executeUpdate();
			
			PreparedStatement st5=null;
			String sql5="DELETE exchange_detail,exchange FROM exchange" + 
					" LEFT JOIN exchange_detail ON exchange_detail.exchange_id = exchange.id" + 
					" WHERE exchange.user_id = ?;";
			st5=con.prepareStatement(sql5);
			st5.setInt(1, userId);
			st5.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

}
