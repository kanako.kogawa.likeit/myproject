package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import beans.ExchangeDataBeans;

public class ExchangeDAO {

	//購入情報登録
	public static int insertExchange(ExchangeDataBeans edb)  {
		Connection con = null;
		PreparedStatement st = null;
		//autoIncKey=-1にしているのは、autoIncKeyには負の数が入らないので、負の数にすることで、エラーと判別できるようにするため
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO exchange (user_id,total_price,exchange_date) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, edb.getUserId());
			st.setInt(2, edb.getTotalPrice());
			st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");
			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return autoIncKey ;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}



	//購入時のポイント消費の反映
	public static void updatePossessedScoreByExchange(int score,int id) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		try {
			String sql="UPDATE user SET possessed_score=? WHERE id=?;";
			st=con.prepareStatement(sql);
			st.setInt(1, score);
			st.setInt(2, id);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}




	//購入時に所有アイテムに追加する
	public static void insertPossessedItemByExchange(int userId, int itemId, int amount) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		try {
			String sql="INSERT INTO item_possessed (user_id,item_id,amount) VALUES (?,?,?);";
			st=con.prepareStatement(sql);
			st.setInt(1, userId);
			st.setInt(2, itemId);
			st.setInt(3, amount);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void updatePossessedItemByExchange(int userId, int itemId, int amount) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
		try {
			String sql="UPDATE item_possessed SET amount=? WHERE user_id=? AND item_id=?;";
			st=con.prepareStatement(sql);
			st.setInt(1, amount);
			st.setInt(2, userId);
			st.setInt(3, itemId);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}



	public static ArrayList<ExchangeDataBeans> getExchangeDataBeansByUserId(int userId){
		Connection con=null;
		ArrayList<ExchangeDataBeans> exchangeList=new ArrayList<ExchangeDataBeans>();
		PreparedStatement st=null;

		try {
			con = DBManager.getConnection();
			String sql="SELECT * FROM exchange WHERE user_id=?;";
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ExchangeDataBeans edb = new ExchangeDataBeans();
				edb.setId(rs.getInt("id"));
				edb.setUserId(rs.getInt("user_id"));
				edb.setTotalPrice(rs.getInt("total_price"));
				edb.setExchangeDate(rs.getTimestamp("exchange_date"));
				exchangeList.add(edb);
			}
			return exchangeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

		public static ExchangeDataBeans getExchangeDataBeansById(int id) {
			Connection con=null;
			con=DBManager.getConnection();
			PreparedStatement st=null;
			ExchangeDataBeans edb = new ExchangeDataBeans();
			try {
				String sql="SELECT * FROM exchange WHERE id=?;";
				st=con.prepareStatement(sql);
				st.setInt(1, id);
				ResultSet rs=st.executeQuery();
				while(rs.next()) {
					edb.setId(rs.getInt("id"));
					edb.setUserId(rs.getInt("user_id"));
					edb.setTotalPrice(rs.getInt("total_price"));
					edb.setExchangeDate(rs.getTimestamp("exchange_date"));
				}
				return edb;
			}catch (SQLException e) {
				System.out.println(e.getMessage());
				return null;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}



	}

