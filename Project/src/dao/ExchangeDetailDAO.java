package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.ExchangeDetailDataBeans;
import beans.ItemDataBeans;

public class ExchangeDetailDAO {

	//購入情報登録処理
	public static void insertExchangeDetail(ExchangeDetailDataBeans eddb) {
		Connection con=null;
		con=DBManager.getConnection();
		PreparedStatement st=null;
//		boolean result=false;
			try {
				String sql="INSERT INTO exchange_detail (exchange_id,item_id,amount) VALUES (?,?,?)";
				st=con.prepareStatement(sql);
				st.setInt(1, eddb.getExchangeId());
				st.setInt(2, eddb.getItemId());
				st.setInt(3, eddb.getAmount());
				st.executeUpdate();
//				result=true;
//				return result;
			} catch (SQLException e) {
				e.printStackTrace();
//				return false;
			}finally {
				if(con!=null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}






	}

























	public static ArrayList<ItemDataBeans> getExchangeDataBeansListByExchangeId(int exchangeId){
		Connection con=null;
		con=DBManager.getConnection();
		try {
			String sql="SELECT i.id,i.file_name,i.name,e.amount,i.price" +
					" FROM exchange_detail e" +
					" INNER JOIN item i" +
					" ON e.item_id=i.id" +
					" WHERE e.exchange_id=?;";
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, exchangeId);
			ResultSet rs=st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setFileName(rs.getString("file_name"));
				idb.setName(rs.getString("name"));
				idb.setAmount(rs.getInt("amount"));
				idb.setPrice(rs.getInt("price"));
				itemList.add(idb);
			}
			return itemList;


		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}



	}

}
