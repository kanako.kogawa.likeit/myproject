package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable{
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String loginId;
	private String name;
	private String password;
	private int easyHighScore;
	private int hardHighScore;
	private int possessedScore;



	public int getPossessedScore() {
		return possessedScore;
	}

	public void setPossessedScore(int possessedScore) {
		this.possessedScore = possessedScore;
	}

	public int getEasyHighScore() {
		return easyHighScore;
	}

	public void setEasyHighScore(int easyHighScore) {
		this.easyHighScore = easyHighScore;
	}

	public int getHardHighScore() {
		return hardHighScore;
	}

	public void setHardHighScore(int hardHighScore) {
		this.hardHighScore = hardHighScore;
	}


	public UserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}



}
