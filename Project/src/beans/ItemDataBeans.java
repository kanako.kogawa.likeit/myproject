package beans;

import java.io.Serializable;

	public class ItemDataBeans implements Serializable{
		private int id;
		private String name;
		private String detail;
		private int price;
		private int amount;
		private int count;
		private int type;
		private int subTotal;
		private String fileName;

		public int getSubTotal() {
			return subTotal;
		}
		public void setSubTotal(int subTotal) {
			this.subTotal = subTotal;
		}
		public int getType() {
			return type;
		}
		public void setType(int type) {
			this.type = type;
		}
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
		

		public int getAmount() {
			return amount;
		}
		public void setAmount(int amount) {
			this.amount = amount;
		}
		public ItemDataBeans() {
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDetail() {
			return detail;
		}
		public void setDetail(String detail) {
			this.detail = detail;
		}
		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

	}


