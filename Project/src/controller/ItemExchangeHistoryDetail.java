package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ExchangeDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ExchangeDAO;
import dao.ExchangeDetailDAO;

/**
 * Servlet implementation class ItemExchangeHistoryDetail
 */
@WebServlet("/ItemExchangeHistoryDetail")
public class ItemExchangeHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		int getId=Integer.parseInt(request.getParameter("id"));
		ExchangeDataBeans exchangeData=ExchangeDAO.getExchangeDataBeansById(getId) ;
		request.setAttribute("data", exchangeData);
		ArrayList<ItemDataBeans>exchangeDataDetail =ExchangeDetailDAO.getExchangeDataBeansListByExchangeId(getId);
		request.setAttribute("list", exchangeDataDetail);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/itemExchangeHistoryDetail.jsp");
		dispatcher.forward(request, response);
	}
}
