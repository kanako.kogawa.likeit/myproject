package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class ItemExchange
 */
@WebServlet("/ItemExchange")
public class ItemExchange extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		if(request.getParameter("error") != null) {
			if(request.getParameter("error").equals( "1")) {
				request.setAttribute("errMsg", "アイテムが選択されていません。");
			}
			if(request.getParameter("error").equals("2")) {
				request.setAttribute("errMsg", "既にカートに入っています。");
			}
		}
		UserDataBeans userData = UserDAO.getUserDataBeansbyUserId(user.getId());
		request.setAttribute("userData", userData);

		ArrayList<ItemDataBeans> colorItem = ItemDAO.getColorItembyUserId(user.getId());
		request.setAttribute("colorItem", colorItem);
		ArrayList<ItemDataBeans> usedupItem = ItemDAO.getUsedupItem();
		request.setAttribute("usedupItem", usedupItem);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemExchange.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//TODO
		//鉛筆→カートに追加されていたら、「そのアイテムは既にカートに追加されています」と表示
		//使い切り→カートに追加されていたら、数量のみ増やす（ただし、5個を超えた場合は「一度に5個まで購入できます」とエラーメッセージを出す）

		HttpSession session = request.getSession();
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
		}

		// 鉛筆のID
		String[] getColorId = request.getParameterValues("color");


		boolean check=ExchangeHelper.isOverLapColorIdByCart(cart, getColorId);
		//既にカートに追加されているか検証
		if(check) {
			response.sendRedirect("ItemExchange?error=2");
			return;
		}
		// 鉛筆
		int[] colorId = null;
		if (getColorId != null) {
			colorId = Stream.of(getColorId).mapToInt(Integer::parseInt).toArray();

			for (int i : colorId) {
				ItemDataBeans colorData = ItemDAO.getItemDataBeansById(i);
				int price=colorData.getPrice();
				int subTotal=price;
				colorData.setSubTotal(subTotal);
				colorData.setAmount(1);
				cart.add(colorData);
			}
		}

		// 使い切りのリスト
		ItemDAO dao = new ItemDAO();
		List<ItemDataBeans> itemList = dao.getUsedupItem();
		//
		boolean buyFlag = false;
		// 使い切りアイテム
		for (ItemDataBeans itemDataBeans : itemList) {
			int quantity = Integer.parseInt(request.getParameter(itemDataBeans.getId() + "-quantity"));
			if (quantity == 0) {
				continue;
				//continueは、for文の最初に戻すということ。
			}
			itemDataBeans.setAmount(quantity);
			int price=itemDataBeans.getPrice();
			int amount=itemDataBeans.getAmount();
			int subTotal=price*amount;
			itemDataBeans.setSubTotal(subTotal);


			// 重複の場合個数追加
			boolean addCartFlag = false;
			for (int i = cart.size()-1; i >= 0; i--) {
				if(cart.get(i).getId() == itemDataBeans.getId()) {
					cart.get(i).setAmount(cart.get(i).getAmount() + amount);
					cart.get(i).setSubTotal(cart.get(i).getSubTotal()+subTotal);
					addCartFlag = true;
				}
			}

			if(!addCartFlag) {
				cart.add(itemDataBeans);
			}
			buyFlag = true;
		}
		//何も選ばれていない状態でカートへ追加を押された場合、エラーメッセージを表示
		if(!buyFlag && getColorId == null) {
			response.sendRedirect("ItemExchange?error=1");
			return;
		}
		session.setAttribute("cart", cart);
		response.sendRedirect("ItemCart");

	}

}
