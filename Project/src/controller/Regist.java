package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;

/**
 * Servlet implementation class SignupServlet
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String inputLoginId=request.getParameter("loginId");
		String inputUserName=request.getParameter("userName");
		String inputPassword=request.getParameter("password");
		String inputConfirmPassword=request.getParameter("confirmPassword");
		String errorMessage="";

		UserDAO userDao=new UserDAO();
		boolean validationId=userDao.isLoginIdValidation(inputLoginId);
		boolean validationName=userDao.isNameValidation(inputUserName);
		boolean validationPassword=userDao.isPasswordValidation(inputPassword, inputConfirmPassword);
		boolean validationOverlapLoginId=userDao.isOverlapLoginId(inputLoginId);

		//パスワードの入力規則チェック 8文字以上のみ入力可能
		if(!validationPassword) {
			errorMessage="パスワードを8文字以上入力してください。";
		}
		//パスワードとパスワード（確認）の入力内容が異なる場合
		if(!(inputPassword.equals(inputConfirmPassword))){
			errorMessage="パスワードとパスワード（確認）が異なります。";
		}
		//ログインIDの入力規則チェック 英数字（4文字以上)のみ入力可能
		if(!validationId) {
			errorMessage="ログインIDは半角英数字で4文字以上入力してください。";
		}
		//名前の入力規則チェック 2文字以上のみ入力可能
		if(!validationName) {
			errorMessage="ユーザ名を2文字以上入力してください。";
		}
		//ログインIDの重複チェック
		if(validationOverlapLoginId) {
			errorMessage="既にそのログインIDは使用されています。";
		}
		//未入力項目がある場合
		if(inputLoginId.equals("")||inputUserName.equals("")||inputPassword.equals("")||inputConfirmPassword.equals("")) {
			errorMessage="未入力の項目があります。";
		}

		if(errorMessage.length()==0) {
			int getId=userDao.insertUser(inputLoginId,inputUserName,inputPassword);
			userDao.addData(getId);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/registComplete.jsp");
			dispatcher.forward(request, response);

		}else {
			request.setAttribute("loginId", inputLoginId);
			request.setAttribute("userName",inputUserName);
			request.setAttribute("errorMessage", errorMessage);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
		}

	}

}
