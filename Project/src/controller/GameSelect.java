package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class GameSelect
 */
@WebServlet("/GameSelect")
public class GameSelect extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		if(user==null) {
			ArrayList<ItemDataBeans> allColorList=ItemDAO.getColorItem();
			request.setAttribute("colorList", allColorList);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/gameSelect.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//カラーアイテムの情報を取得
		ArrayList<ItemDataBeans> allColorList=ItemDAO.getColorItem();
		ArrayList<ItemDataBeans> possessedColorList=UserDAO.getPossessedColorItemsByUserId(user.getId());
		for(ItemDataBeans allColorItem:allColorList) {
			for(ItemDataBeans possessedColorItem :possessedColorList ) {
				if(allColorItem.getId()==possessedColorItem.getId()) {
					allColorItem.setAmount(possessedColorItem.getAmount());
					continue;
				}
			}
		}
		request.setAttribute("colorList", allColorList);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/gameSelect.jsp");
		dispatcher.forward(request, response);
	}
}
