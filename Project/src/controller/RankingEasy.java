package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.RankingDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class RankingEasy
 */
@WebServlet("/RankingEasy")
public class RankingEasy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RankingEasy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	ArrayList<RankingDataBeans> rankingList=UserDAO.getEasyRanking();
	request.setAttribute("rankingList", rankingList);
	RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/rankingEasy.jsp");
	dispatcher.forward(request, response);



		//SQLに、INSERT...ON DUPLICATE KEY UPDATE を使う
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
