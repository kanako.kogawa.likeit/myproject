package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		String getDeleteId=request.getParameter("id");
		int deleteId=Integer.parseInt(getDeleteId);
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		for (ItemDataBeans cartInItem : cart) {
			if (cartInItem.getId() == deleteId) {
				cart.remove(cartInItem);
				int total=ExchangeHelper.getTotal(cart);
				session.setAttribute("total",total);
				break;
			}
		}
		response.sendRedirect("ItemCart");
	}
}
