package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		UserDataBeans userData=UserDAO.getUserDataBeansbyUserId(user.getId());
		request.setAttribute("score", userData.getPossessedScore());
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		UserDAO.userDelete(user.getId());
		session.removeAttribute("userInfo");
		response.sendRedirect("DeleteComplete");

	}
}
