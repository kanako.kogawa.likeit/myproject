package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ExchangeDataBeans;
import beans.UserDataBeans;
import dao.ExchangeDAO;

/**
 * Servlet implementation class ItemExchangeHistory
 */
@WebServlet("/ItemExchangeHistory")
public class ItemExchangeHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		List<ExchangeDataBeans> exchangeList=ExchangeDAO.getExchangeDataBeansByUserId(user.getId());
		request.setAttribute("exchangeList", exchangeList);
		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/itemExchangeHistory.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
