package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ExchangeDataBeans;
import beans.ExchangeDetailDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import beans.UserPossessedDataBeans;
import dao.ExchangeDAO;
import dao.ExchangeDetailDAO;
import dao.UserDAO;

/**
 * Servlet implementation class ExchangeConfirm
 */
@WebServlet("/ExchangeConfirm")
public class ExchangeConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		if(request.getParameter("error")!=null) {
			request.setAttribute("errMsg", "スコアが不足しています");
		}
		UserDataBeans userData = UserDAO.getUserDataBeansbyUserId(user.getId());
		request.setAttribute("userData", userData);
		ArrayList<ItemDataBeans> cart= (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		int itemTypeCount=cart.size();
		request.setAttribute("itemTypeCount", itemTypeCount);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/exchangeConfirm.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		ExchangeDataBeans edb = new ExchangeDataBeans();
		edb.setUserId(user.getId());
		edb.setTotalPrice((int)session.getAttribute("total"));
		//所持ポイントと消費ポイントを比較する。所持ポイントの方が少なかった場合エラーを表示
		//この段階に置くことで、 ExchangeHelper.cutSessionAttributeでカートのセッション切を防ぐことができる
		UserDataBeans loginUser=UserDAO.getUserDataBeansbyUserId(user.getId());
		int score=loginUser.getPossessedScore()-edb.getTotalPrice();
		if(score<0) {
			response.sendRedirect("ExchangeConfirm?error=1");
			return;
		}
		// セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) ExchangeHelper.cutSessionAttribute(session, "cart");
		//cart はsession.getAttribute("cart")を実行したときの値

		// 購入情報を登録
		int exchangeId = ExchangeDAO.insertExchange(edb);
		// 購入詳細情報を購入情報IDに紐づけして登録
		for (ItemDataBeans cartInItem : cart) {
			ExchangeDetailDataBeans eddb = new ExchangeDetailDataBeans();
			eddb.setExchangeId(exchangeId);
			eddb.setItemId(cartInItem.getId());
			eddb.setAmount(cartInItem.getAmount());
			ExchangeDetailDAO.insertExchangeDetail(eddb);
		}


		//所持ポイントから購入時消費ポイントを引き、データベースに反映させる
		ExchangeDAO.updatePossessedScoreByExchange(score, user.getId());
		//購入完了後所持アイテムに追加
		//方針：select文でnullかどうか判別（ログイン時にユーザーがいるかどうか判別するときと同じような感じで実装する）
			for(ItemDataBeans cartInItem:cart) {
				boolean result=UserDAO.isExistPossessedItemByUserId(user.getId(), cartInItem.getId());
				if(!result) {
					ExchangeDAO.insertPossessedItemByExchange(user.getId(), cartInItem.getId(), cartInItem.getAmount());
				}else {
					UserPossessedDataBeans updb=UserDAO.getUserPossessedDataBeansByUserIdAndItemId(user.getId(), cartInItem.getId());
					int updateAmount=updb.getAmount()+cartInItem.getAmount();
					ExchangeDAO.updatePossessedItemByExchange(user.getId(),cartInItem.getId(),updateAmount);
				}
			}

		// 購入完了ページ
		response.sendRedirect("ExchangeComplete");

	}
}


