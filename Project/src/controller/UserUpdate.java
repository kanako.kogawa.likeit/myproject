package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		UserDataBeans userData=UserDAO.getUserDataBeansbyUserId(user.getId());
		request.setAttribute("userData",userData);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session=request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		int id=user.getId();
		String loginId=request.getParameter("loginId");
		String password=request.getParameter("inputPassword");
		String confirmPassword=request.getParameter("confirmPassword");
		String userName=request.getParameter("userName");
		String validationMessage="";

		UserDAO userDao=new UserDAO();
		boolean validationId=userDao.isLoginIdValidation(loginId);
		boolean validationName=userDao.isNameValidation(userName);
		boolean validationPassword=userDao.isPasswordValidation(password, confirmPassword);
		boolean validationOverlapLoginId=userDao.isOverlapLoginIdById(id, loginId);

		//パスワードの入力規則チェック 8文字以上のみ入力可能
		if(!validationPassword) {
			validationMessage="パスワードを8文字以上入力してください。";
		}
		//パスワードとパスワード（確認）の入力内容が異なる場合
		if(!(password.equals(confirmPassword))){
			validationMessage="パスワードとパスワード（確認）が異なります。";
		}
		//ログインIDの入力規則チェック 英数字（4文字以上)のみ入力可能
		if(!validationId) {
			validationMessage="ログインIDは半角英数字で4文字以上入力してください。";
		}
		//名前の入力規則チェック 2文字以上のみ入力可能
		if(!validationName) {
			validationMessage="ユーザ名を2文字以上入力してください。";
		}
		//ログインIDの重複チェック
		if(validationOverlapLoginId) {
			validationMessage="既にそのログインIDは使用されています。";
		}

		//ログインIDと名前の入力規則を満たし、ログインIDの重複チェックがクリアされている状態で、ログインIDか名前が変更された場合
		if(validationId && validationName && !validationOverlapLoginId && password.equals("") && confirmPassword.equals("")){
			userDao.updateUserWithoutPassword(loginId,userName,id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/updateComplete.jsp");
			dispatcher.forward(request, response);
		//パスワード変更の場合
		}else if(validationMessage.equals("")) {
			userDao.updateUser(loginId,password,userName,id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/updateComplete.jsp");
			dispatcher.forward(request, response);
		//エラーの場合
		}else {
			UserDataBeans userData=userDao.getUserDataBeansbyUserId(user.getId());
			request.setAttribute("userData",userData);
			request.setAttribute("validationMessage", validationMessage);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}
	}
}
