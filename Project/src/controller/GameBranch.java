package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GameBranch
 */
@WebServlet("/GameBranch")
public class GameBranch extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String level=request.getParameter("level");
		String getColor=request.getParameter("color");
		int color=Integer.parseInt(getColor);

		if(level.equals("かんたん")) {
			request.setAttribute("colorId",color);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/gameEasyLevel.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(level.equals("むずかしい")) {
			request.setAttribute("color",color);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/gameHardLevel.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}
}
