package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class ItemPossessed
 */
@WebServlet("/ItemPossessed")
public class ItemPossessed extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		//カラーアイテムの情報を取得
		ArrayList<ItemDataBeans> allColorList=ItemDAO.getColorItem();
		ArrayList<ItemDataBeans> possessedColorList=UserDAO.getPossessedColorItemsByUserId(user.getId());
		for(ItemDataBeans allColorItem:allColorList) {
			for(ItemDataBeans possessedColorItem :possessedColorList ) {
				if(allColorItem.getId()==possessedColorItem.getId()) {
					allColorItem.setAmount(possessedColorItem.getAmount());
					continue;
				}
			}
		}
		request.setAttribute("colorList", allColorList);

		//消費アイテムの情報を取得
		ArrayList<ItemDataBeans> allUsedupItemList=ItemDAO.getUsedupItem();
		ArrayList<ItemDataBeans> possessedUsedupItemList=UserDAO.getPossessedUsedupItemsByUserId(user.getId());
		for(ItemDataBeans allUsedupItem:allUsedupItemList) {
			for(ItemDataBeans possessedUsedupItem:possessedUsedupItemList) {
				if(allUsedupItem.getId()==possessedUsedupItem.getId()) {
					allUsedupItem.setAmount(possessedUsedupItem.getAmount());
					continue;
				}
			}
		}
		request.setAttribute("itemList", allUsedupItemList);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/itemPossessed.jsp");
		dispatcher.forward(request, response);
	}
}
