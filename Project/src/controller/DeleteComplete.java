package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class DeleteComplete
 */
@WebServlet("/DeleteComplete")
public class DeleteComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/deleteComplete.jsp");
		dispatcher.forward(request, response);
	}
}
