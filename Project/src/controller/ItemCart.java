package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;

/**
 * Servlet implementation class ItemCart
 */
@WebServlet("/ItemCart")
public class ItemCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user =(UserDataBeans) session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("GameTitle");
			return;
		}
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
			session.setAttribute("cart", cart);
		}
		String cartActionMessage = "";
		//カートに商品が入っていないなら
		if(cart.size() == 0) {
			cartActionMessage = "カートに商品がありません。";
		}
		//itemExchangeのpostに書いてしまうと、購入したあと戻ってきたときに、cartを作成してしまうため、
		//session.setAttribute("total",total);が切れない。
		//ここに書くことで、購入したときにcartのセッションが切れているので、それと連動してtotalには0が入る。
		//合計金額を取得
		int total=ExchangeHelper.getTotal(cart);
		session.setAttribute("total",total);
		request.setAttribute("cartActionMessage", cartActionMessage);
		request.getRequestDispatcher("/WEB-INF/jsp/itemCart.jsp").forward(request, response);
	}
}
