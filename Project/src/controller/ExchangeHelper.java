package controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

public class ExchangeHelper {

	//合計を求めるメソッド
	public static int getTotal(ArrayList<ItemDataBeans> cart) {
		int total = 0;
		for (ItemDataBeans item : cart) {
			total += item.getSubTotal();
		}
		return total;
	}

	//カートにカラーアイテムがすでに入っているかどうか判別するメソッド
	public static boolean isOverLapColorIdByCart(ArrayList<ItemDataBeans> cart, String[] colorId) {
		boolean result = false;
		//何も選択されていない状態（colorIdに何も入っていない状態）でこのメソッドが実行されると、
		//List<String> selectedId = Arrays.asList(null);になってしまいエラーが出るため、
		//if文で条件を付ける
		if (colorId != null) {
			for(String id:colorId) {
				for (ItemDataBeans item : cart) {
					String cartId = Integer.toString(item.getId());
					if (cartId.equals(id)) {
						result = true;
						return result;
					}
				}
			}
		}
		//このように書くこともできる。
//		if (colorId != null) {
//		List<String> selectedId = Arrays.asList(colorId);
//		for (ItemDataBeans item : cart) {
//			String cartId = Integer.toString(item.getId());
//			if (selectedId.contains(cartId)) {
//				result = true;
//				return result;
//			}
//		}
//	}
		return result;
	}

	//セッションから指定データを取得（削除も一緒に行う）
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}
	

	
	
	
}
