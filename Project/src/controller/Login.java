package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("logout") != null) {
			request.setAttribute("infoMsg", "ログアウトしました。");
		}
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				request.setCharacterEncoding("UTF-8");
				String loginId = request.getParameter("loginId");
				String password = request.getParameter("password");
				UserDAO userDao = new UserDAO();
				UserDataBeans user = userDao.getUserId(loginId, password);
				/** テーブルに該当のデータが見つからなかった場合 **/
				if (user == null) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
					dispatcher.forward(request, response);
					return;
				}

				HttpSession session = request.getSession();
				session.setAttribute("userInfo", user);
				response.sendRedirect("GameSelect");
	}

}
