<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>


<body>
    <br>
    <h1 class="title">ユーザ新規登録</h1>


    <div class="screenConfig">

        <form method="post" action="Regist">
            <div class="form-group row">
                <label for="inputID" class="col-sm-4 col-form-label">ログインID</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputID" name="loginId" value="${loginId}" >
                </div>
            </div>
            <div class="form-group row">
                <label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="userName" name="userName" value="${userName}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-4 col-form-label">パスワード</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputPassword" name="password">
                </div>
            </div>

            <div class="form-group row">
                <label for="confirmPassword" class="col-sm-4 col-form-label">パスワード(確認）</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">
                </div>
            </div>


            <br>
            <div class="submit">
                <input class="registerButton" type="submit" value="登録">
            </div>
        </form>
           <br> <p class="alert-red"><c:if test="${errorMessage !=null}">${errorMessage}</c:if></p>
           <a href="GameTitle">戻る</a>
        </div>


</body></html>