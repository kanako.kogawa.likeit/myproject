<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>アイテム交換所</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link
	href="https://fonts.googleapis.com/css?family=Noto+Serif+JP&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="screenConfig">
		<div align="right">
			<a href="Logout">ログアウト</a>
		</div>
		<p align="right">
			<a href="Mypage"></a>
		</p>

		<h1>
			アイテム交換所<a href="ItemCart"><img src="image/l_e_company_564.png"
				class="cart-image" align="right"></a>
		</h1>
		<p>
			<a href="Mypage">マイページ</a>
		</p>
		<p>
			ここでは、獲得した累計スコア（保有スコア）を使って<br> アイテムと交換することができます！
		</p>
		<p>${userInfo.name}さんの保有スコア${userData.possessedScore}pt</p>
		<c:if test="${errMsg != null}">
			<p class="alert-red">${errMsg}</p>
		</c:if>
		<form method="post" action="ItemExchange">
			★ゲーム選択画面でゲーム中の漢字の文字色の変更をすることができます。★

			<div class="item-area">
				<div class="row">
					<c:forEach var="color" items="${colorItem}">
						<div class="col-4">
							<div>
								<label for="${color.name}"> <img
									src="image/${color.fileName}" class="colorSelect">
								</label>
							</div>
							<div>
								${color.name}

								<c:if test="${color.count == 0}">
									<input type="checkbox" name="color" value="${color.id}"
										id="${color.name}">
								</c:if>
								<c:if test="${color.count != 0}">
									<input type="checkbox" name="color" value="${color.id}"
										id="${color.name}" disabled>
								</c:if>
							</div>
							<c:if test="${color.count == 0}">
								<div>${color.price}pt</div>
							</c:if>
							<c:if test="${color.count != 0}">
								<div>購入済み</div>
							</c:if>
						</div>
					</c:forEach>
				</div>
			</div>
			<br> ★ゲーム中に使用できるアイテムです。★
			<c:forEach var="item" items="${usedupItem}">
				<div class="row item-description">
					<div class="col-5">
						<img src="image/${item.fileName}" class="item-description-image">
					</div>
					<div class="col-7 left-align">
						${item.name}<br>${item.detail}<br>${item.price}pt/個<br>
						<select name="${item.id}-quantity">
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</div>
			</c:forEach>
			<input type="submit" value=カートへ追加>
		</form>

	</div>
</body>
</html>