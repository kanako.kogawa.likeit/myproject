<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
        <h1>ゲーム終了！！</h1>
        <p>テストさんのスコアは・・・</p>
        <p>
            <font size="25px">
                <font color="#de4e4e">10000</font>でした！
            </font>
        </p>
        <!--ランキング100位以内に入った場合のみ表示-->
        <p>
            <font size="5px" color="yellow">ランキング3位おめでとう！</font>
        </p>
        <p>登録日時 2020年01月01日12時34分</p>
        <p><a href="rankingHard.html" class="left-side">ランキング一覧</a>
            <a href="gameSelect.html" class="right-side">もう一回遊ぶ？</a></p>
        <a href="#">ログアウト</a>
    </div>
</body></html>