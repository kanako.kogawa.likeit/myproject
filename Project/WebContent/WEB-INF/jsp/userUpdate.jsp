<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>マイページ</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <div class="screenConfig">
        <h1>ユーザ情報更新</h1>
        <form method="post" action="UserUpdate">


            <div class="form-group row">
                <label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" id="loginId" value="${userData.loginId}" name="loginId">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-4 col-form-label">パスワード</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputPassword" name="inputPassword">
                </div>
            </div>

            <div class="form-group row">
                <label for="confirmPassword" class="col-sm-4 col-form-label">パスワード(確認）</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">
                </div>
            </div>

            <div class="form-group row">
                <label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="userName" value="${userData.name}" name="userName">
                </div>
            </div>
            <br>
            <div class="submit">
                <input class="registerButton" type="submit" value="更新">
            </div>
        </form>

        <br>
        <br>

        <p class="alert-red"><c:if test="${validationMessage !=null}">${validationMessage}</c:if></p><br>


        <a href="Mypage">戻る</a>






    </div>





</body></html>
