<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>アイテム交換所</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link
	href="https://fonts.googleapis.com/css?family=Noto+Serif+JP&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>

<body>
	<div class="screenConfig">
		<div align="right">
			<a href="Logout">ログアウト</a>
		</div>
		<h1>保有アイテム一覧</h1>

		★ゲーム選択画面でゲーム中の漢字の文字色の変更をすることができます。★
		<div class="item-area">
			<div class="row">
				<div class="col-6">
					<div>
						<img src="image/white.png" class="colorSelect">
					</div>
					<div>白色</div>
				</div>
				<c:forEach var="color" items="${colorList}">
					<div class="col-6">
						<div>
							<img src="image/${color.fileName}" class="colorSelect">
						</div>
						<div>${color.name}</div>
						<c:if test="${color.amount==1}">
							<div>購入済み</div>
						</c:if>
						<c:if test="${color.amount!=1}">
							<div>
								<a href="ItemExchange">購入する</a>
							</div>
						</c:if>
					</div>
				</c:forEach>
			</div>
		</div>
		<br>

		<div class="item-area">
			<div class="row">
			<c:forEach var="item" items="${itemList}">
				<div class="col-4">
					<div>
						<img src="image/${item.fileName}" class="items">
					</div>
					<div>${item.name}</div>
					<div>${item.amount}個所持</div>
					<div>
						<a href="ItemExchange">購入する</a>
					</div>
				</div>
				</c:forEach>
			</div>
		</div>
		<br>
		<br> <a href="Mypage">戻る</a>



	</div>
</body>
</html>