<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>カート</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="screenConfig">
		<!--画面の中央に持ってくる-->
		<h1>カート</h1>
		<p class="alert-red">${cartActionMessage}</p>
			<table class="exchange-table" border="1">
				<tr>
					<th width="230"></th>
					<th width="200" class="left-align">アイテム名</th>
					<th width="150">必要スコア</th>
				</tr>
				<c:forEach var="item" items="${cart}">
					<tr height="120">
						<td><img src="image/${item.fileName}" class="items"></td>
						<td>
							<div class="left-align">${item.name}
								<div>
									<div class="row">
										<div class="col-4">
											<a href="ItemDelete?id=${item.id}">削除</a>
										</div>
										<c:if test="${item.type !=1}">
											<div class="col-8">
												数量${item.amount}
												<!--
												<select name="quantity">
													<option value="0">0（削除）</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
 												-->
											</div>
										</c:if>

									</div>
								</div>
							</div>
						</td>
						<td>${item.subTotal}pt</td>
					</tr>
				</c:forEach>
				<tr class="subtotal">
					<td>合計</td>
					<td></td>
					<td>${total}pt</td>
				</tr>
			</table>
			<br> <a href="ExchangeConfirm"><input type="button" value="レジに進む"></a>
		<br><br>
		<p>
			<a href="ItemExchange">戻る</a>
		</p>
	</div>
</body>
</html>