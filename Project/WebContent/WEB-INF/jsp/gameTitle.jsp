<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ゲームスタート</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
    <h1>
        ようこそ
    </h1>
    <br>
    <p>
        新規登録は<a href="Regist">こちら</a>
    </p>
    <br>
    <p>
        登録済みの方は<a href="Login">ログイン</a>
    </p>
    <br>
    <p>
        ゲストプレイする方は<a href="GameSelect">こちら</a>
        <br>
        (※ゲストプレイでは、HARD LEVELのプレイ及びスコアの記録ができません！)
        <br>
    </p>
    </div>
</body></html>