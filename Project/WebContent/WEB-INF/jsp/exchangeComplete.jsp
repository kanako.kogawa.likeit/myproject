<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>交換完了画面</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
        <!--画面の中央に持ってくる-->
        <div class="center-position">
            <p>交換完了しました。</p>
        </div>
        <p><a href="Mypage">マイページへ</a></p>
        <p><a href="ItemExchange">引き続き交換する</a></p>
    </div>
</body></html>