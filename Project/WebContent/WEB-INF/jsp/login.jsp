<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="screenConfig">
		<h1 class="title">ログイン画面</h1>
		<!--ログアウト時にのみ出現するメッセージ-->
		<c:if test="${infoMsg != null}">
			<p>
				<font color="pink">ログアウトしました</font>
			</p>
		</c:if>

		<form method="post" action="Login">
			<div>
				<label for="inputId">ログインID</label>
				<div>
					<input type="text" class="loginForm" id="inputId" name="loginId">
				</div>
			</div>
			<div>
				<label for="inputPassword">パスワード</label>
				<div>
					<input type="password" class="loginForm" id="inputPassword"
						name="password">
				</div>
			</div>
			<div>
				<input class="loginButton" type="submit" value="ログイン">
			</div>
		</form>
		<!--ログイン失敗時のみ出現するメッセージ-->
		<c:if test="${errMsg != null}">
			<p class="alert-red">${errMsg}</p>
		</c:if>
		<br> <a href="GameTitle">戻る</a>
	</div>
</body>
</html>