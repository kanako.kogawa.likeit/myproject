<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>魚編の漢字</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
     <link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP:500,700&display=swap&subset=japanese" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
        <h1>easymode</h1>
        <p> <a href="gameEasyLevel.html">リスタートする</a></p>
        <p>スコア　100</p>
        <div>
            <div class="easyletter color-${colorId}">
                鮑鯏鯵鮎𩺊<br>
                鮑鮟鰯鮇鮑<br>
                鮑鯏鯵鮎𩺊<br>
                鮑鮟鰯鮇鮑<br>
                鮑鯏鯵鮎𩺊<br>
                鮑鮟鰯鮇鮑<br>
                鮑鯏鯵鮎𩺊<br>
                鮑鮟鰯鮇鮑<br>
            </div>
            制限時間のバーを付ける！（JavaScript)<br>
            <hr class="timer">
            ・制限時間バーの下に、使い切りアイテムを表示（1回につき2個まで使用可能）させる<br>
            <img src="image/hint.png" class="items">
            <img src="image/reset.png" class="items">
            <img src="image/booster.png" class="items">
        </div>
    </div>

</body></html>