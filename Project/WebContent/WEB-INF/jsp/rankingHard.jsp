<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link
	href="https://fonts.googleapis.com/css?family=Noto+Serif+JP&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="screenConfig">
		<div align="right">
			<a href="Logout">ログアウト</a>
		</div>
		<h1>ランキング（むずかしい）</h1>

		<p align="right">
			<a href="Mypage">マイページ</a>
		</p>
		<p>
			ランキング（かんたん）は<a href="RankingEasy">こちら</a> <br>※スコアが0の場合ランキングに表示されません。
			<br><a href="GameSelect">ゲーム選択画面</a>
		</p>
		<table class="tableArea" border="1">
			<tr>
				<th width="150">ユーザ名</th>
				<th width="250">スコア</th>
				<th width="200">スコア登録日時</th>
			</tr>
			<c:forEach var="user" items="${rankingList}">
				<c:if test="${user.score>0}">
					<tr>
						<td>${user.userName}</td>
						<td>${user.score}</td>
						<td>${user.formatDate}</td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
</body>
</html>