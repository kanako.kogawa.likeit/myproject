<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>交換確認</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="screenConfig">
		<h1>交換確認</h1>
		<p>保有スコア${userData.possessedScore}pt</p>
		<c:if test="${errMsg !=null}"><p class="alert-red">保有スコアが足りません！</p>
        <p><a href="GameSelect">ゲームをして稼ぐ！</a></p>
</c:if>
			<table class="exchange-table" border="1">
				<tr>
					<th width="230"></th>
					<th width="200">アイテム名</th>
					<th width="50">数量</th>
					<th width="150">小計</th>
				</tr>
				<c:forEach var="item" items="${cart}">
					<tr height="120">
						<td><img src="image/${item.fileName}" class="items"></td>
						<td>${item.name}</td>
						<td>${item.amount}</td>
						<td>${item.subTotal}pt</td>
					</tr>
				</c:forEach>
				<tr class="subtotal">

					<td>合計</td>
					<td></td>
					<td></td>
					<td>${total}pt</td>
				</tr>
			</table>
			<br>
			<p>以上${itemTypeCount}点を交換しますか?</p>
			<div class="row">
				<div class="col-6">
					<a href="ItemExchange"><input class="cancelButton" type="submit"
						value="キャンセル"></a>
				</div>
				<div class="col-6">
				<form action="ExchangeConfirm" method="post">
					<input type="submit" value="交換する">
					</form>
				</div>
			</div>
		<br>
	</div>
</body>
</html>