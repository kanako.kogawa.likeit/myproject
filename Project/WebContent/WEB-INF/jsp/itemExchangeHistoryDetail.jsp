<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>アイテム交換履歴詳細</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
        <div align="right"><a href="Logout">ログアウト</a></div>
        <h1>アイテム交換履歴詳細</h1>
        <table class="exchange-table" border="1">
            <tr>
                <th width="150">交換日時</th>
                <th width="150">交換スコア</th>
            </tr>
            <tr>
                <td>${data.formatDate}</td>
                <td>${data.totalPrice}pt</td>
            </tr>
        </table>
        <br>
        <table class="exchange-table" border="1">
            <tr>
                <th width="100"></th>
                <th width="100">アイテム名</th>
                <th width="50">個数</th>
                <th width="50">単価</th>
            </tr>
            <c:forEach var="item" items="${list}">
            <tr height="120">
                <td><img src="image/${item.fileName}" class="items"></td>
                <td>${item.name}</td>
                <td>${item.amount}</td>
                <td>${item.price}</td>
            </tr>
            </c:forEach>
        </table>
        <br><br>
        <a href="ItemExchangeHistory">戻る</a>
    </div>
</body></html>