<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ゲームモード選択</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<br>
	<div class="screenConfig">
		<div align="right">
			<p align="right">
				<c:if test="${userInfo != null}"> ${userInfo.name} </c:if>
				<c:if test="${userInfo == null}">ゲスト</c:if>
				さん
				<!--ログインセッションがある場合は、名前を表示-->
			</p>
			<c:if test="${userInfo != null}">
				<a href="Logout">ログアウト</a>
			</c:if>


		</div>
		<h1 class="title">難易度選択</h1>
		<!--位置をもう少し右へ-->
		<!--ログインセッションがある場合はリンクを表示-->


		<div align="right">
			<c:if test="${userInfo != null}">
				<a href="Mypage">マイページ</a>
			</c:if>
		</div>


		<p>文字色を選択してください （※ゲストプレイの場合、白のみとなります。）</p>
		<!--ゲストプレイ時は白以外のラジオボタンが機能しないようにする-->

		<form method="get" action="GameBranch">
			<div class="item-area">
				<div class="row">
					<div class="col-6">
						<div>
							<label for="white"><img src="image/white.png"
								class="colorSelect"></label>
						</div>
						<div>
							白色<input type="radio" name="color" value="0" id="white" checked>
						</div>
					</div>

					<c:if test="${userInfo==null}">
					<c:forEach var="color" items="${colorList}">
					<div class="col-6">
						<div>
							<label for="${color.id}"><img src="image/${color.fileName}"
								class="colorSelect"></label>
						</div>
						<div>
							${color.name}<input type="radio" name="color" value="${color.id}" id="${color.id}"disabled>
						</div>
					</div>
					</c:forEach>
					</c:if>

					<c:if test="${userInfo!=null}">
					<c:forEach var="color" items="${colorList}">
					<div class="col-6">
						<div>
							<label for="${color.id}"><img src="image/${color.fileName}"
								class="colorSelect"></label>
						</div>
						<c:if test="${color.amount!=0}">
						<div>
							${color.name}<input type="radio" name="color" value="${color.id}" id="${color.id}">
						</div>
						</c:if>
						<c:if test="${color.amount ==0}">
						<div>
							${color.name}<input type="radio" name="color"value="${color.id}" id="${color.id}" disabled>
						</div>
						</c:if>
					</div>
					</c:forEach>
					</c:if>
				</div>
			</div>
			<br>

			<p>難易度を選んでください(クリックしてスタート)</p>

			<div class="row">
                <div class="col-6"><input type="submit" class="easy-button" value="かんたん" name="level"></div>
                <div class="col-6"><input type="submit" class="hard-button" value="むずかしい" name="level"></div>
            </div>

		</form>
		<p>
			<a href="GameTitle">戻る</a>
		</p>

	</div>



</body>
</html>