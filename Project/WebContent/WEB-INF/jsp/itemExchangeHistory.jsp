<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>アイテム交換履歴</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="screenConfig">
        <div align="right"><a href="#">ログアウト</a></div>
        <h1>アイテム交換履歴</h1>
                    <table class="exchange-table" border="1">
        <tr>
            <th width="230">交換日時</th>
            <th width="150">交換スコア</th>
            <th width="50"></th>
        </tr>
        <c:forEach var="list" items="${exchangeList}">
        <tr>
            <td>${list.formatDate}</td>
            <td>${list.totalPrice}pt</td>
            <td><a href="ItemExchangeHistoryDetail?id=${list.id}"><input type="submit" value="詳細"></a></td>
        </tr>
         </c:forEach>
        </table>
<br><br>
        <a href="Mypage">戻る</a>
    </div>
</body></html>