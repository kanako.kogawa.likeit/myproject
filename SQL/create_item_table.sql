create table item (id int primary key auto_increment, name varchar(256) not null,price int not null, file_name varchar(256) not null);
alter table item add detail varchar(500) after name;
alter table item rename to color_item;
alter table color_item rename to item;
alter table item add type int;
