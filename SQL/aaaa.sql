SELECT id,name,price,file_name,
(SELECT COUNT(*) FROM item i2 
INNER JOIN exchange_detail exd
ON i2.id = exd.item_id
INNER JOIN  exchange ex 
ON ex.id = exd.exchange_id
WHERE ex.user_id = 1
AND i.id = i2.id
) as count
FROM item i
WHERE type=1;

