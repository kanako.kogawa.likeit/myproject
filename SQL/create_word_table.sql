create table word (id int primary key auto_increment ,`character`  varchar(256), category int);
alter table word add ruby varchar(256);
alter table word modify ruby varchar(256) after `character`;
alter table word convert to character set utf8mb4;
alter table word add chara_file varchar(256)after `character`;
alter table  word modify (chara_file varchar(255) unique,ruby varchar(30),`character` varchar(10));

/*alter table word modify (
chara_file varchar(255),
ruby varchar(10),
'character' varchar(10)
)*/

alter table word change chara_file chara_file varchar(256) CHARACTER SET ascii unique;
