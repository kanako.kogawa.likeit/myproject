create table ranking_hard (id  int primary key auto_increment, user_id int unique, high_score int);
alter table ranking_hard add `date` datetime;
alter table ranking_hard change column high_score hard_high_score int;
