create table user 
(id int primary key auto_increment, name varchar(255) not null,login_id varchar(255) not null unique,login_password varchar(255) not null,possessed_score int);
ALTER TABLE user CHANGE COLUMN login_password password varchar(255) not null;
