create table ranking_easy (id  int primary key auto_increment, user_id int unique, high_score int);
alter table ranking_easy add `date` datetime;
alter table ranking_easy change column high_score easy_high_score int;
